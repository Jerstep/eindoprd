﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveManager : MonoBehaviour {

    public List<Transform> spawners;

    public float cooldown;
    private float spawnCooldown;
    public bool canSpawn;
    public int Enemies;

    public GameObject sergant;

    void Awake() {
        // Event Listners
        Spawner.SendSpawner += AddSpawners;
    }


    private void Start() {
        spawnCooldown = cooldown;
    }


    void Update() {
        if ( canSpawn ) {
            SpawnEnemys();
            print(canSpawn);

            if ( spawnCooldown > 0 ) {
                spawnCooldown -= Time.deltaTime;
            }
            else {
                spawnCooldown = cooldown;
            }
        }
    }


    void AddSpawners(GameObject spawnerObject) {
        spawners.Add(spawnerObject.transform);
        print(spawners);
    }


    void SpawnEnemys() {
        for ( int i = 0; i < Enemies; i++ ) {
            if ( spawnCooldown < 0 ) {
                int spawnPointIndex = Random.Range(0, spawners.Capacity - 1);
                Instantiate(sergant, spawners[spawnPointIndex].transform);
            }
        }
    }
}